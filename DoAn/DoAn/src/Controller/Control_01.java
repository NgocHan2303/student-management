/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Model.LoginModel;
import Model.MSSQLDatabaseConnection;
import View.Home_1;
import View.Home;
import View.LoginForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author MinhDuong
 */
public class Control_01 {

    private LoginForm loginForm;
    private LoginModel loginModel;
    private MSSQLDatabaseConnection sqlConnection;

    public Control_01(LoginForm loginForm, LoginModel loginModel, MSSQLDatabaseConnection sqlConnection) {
        this.sqlConnection = sqlConnection;
        this.loginForm = loginForm;
        this.loginModel = loginModel;
        this.loginForm.btnSignInActionListener(new SignInActionListener());
        this.loginForm.btnSignInGuestActionListener(new SignInGuestActionListener());
    }

    public void showMainForm() {
        Home main = new Home();
        Control_02 ctrl = new Control_02(main);
        loginForm.setVisible(false);
        main.setVisible(true);
    }

    private class SignInGuestActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            loginForm.dispose();
            new Home_1().setVisible(true);
        }
    }

    private class SignInActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String sql = "SELECT * FROM ACCOUNT WHERE TENACC='" + loginForm.getTxtAccount() + "' and PASS='" + loginForm.getTxtPass() + "'";
            ResultSet rs = null;
            try {
                rs = sqlConnection.executeQuery(sql);
            } catch (SQLException ex) {
                Logger.getLogger(Control_01.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                if (rs.next()) {
                    showMainForm();
//                    loginForm.dispose();
//                    Home main = new Home();
//                    main.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(loginForm, "Username or Password Wrong");
                }
            } catch (SQLException ex) {
                Logger.getLogger(Control_01.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void showLoginForm() {
        loginForm.setVisible(true);
    }
}
