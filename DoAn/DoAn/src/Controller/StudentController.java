/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Model.MSSQLDatabaseConnection;
import Model.Student;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author MinhDuong
 */
public class StudentController {

    private ArrayList<Student> students;

    public StudentController(ArrayList<Student> students) {
        this.students = new ArrayList<>();
    }

    public void loadStudentsFromDatabase(MSSQLDatabaseConnection sqlConnection) {
        students.clear();
        try {
            String sql = "SELECT * FROM HOCVIEN";
            ResultSet rs = sqlConnection.executeQuery(sql);

            while (rs.next()) {
                Student student = new Student();
                student.setMaHV(rs.getString("MAHV"));
                student.setHo(rs.getString("HO"));
                student.setTen(rs.getString("TEN"));
                student.setNgSinh(rs.getDate("NGSINH"));
                student.setGioiTinh(rs.getString("GIOITINH"));
                student.setNoiSinh(rs.getString("NOISINH"));
                student.setMaLop(rs.getString("MALOP"));
                student.fetchGradesFromDatabase(sqlConnection);
                student.calAverageDiem();
                student.setAverageDiem(student.getAverageDiem());
                students.add(student);
            }

            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Student> getStudents() {
        return students;
    }
}
