
import Controller.Control_01;
import Model.LoginModel;
import Model.MSSQLDatabaseConnection;
import View.Home;
import View.LoginForm;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

/**
 *
 * @author MinhDuong
 */
public class DoAn_Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        MSSQLDatabaseConnection conn = new MSSQLDatabaseConnection();
        LoginModel model_login = new LoginModel();
        LoginForm view_login = new LoginForm();
        Control_01 ctrl_login = new Control_01(view_login, model_login, conn);
        ctrl_login.showLoginForm();
    }
    
}
