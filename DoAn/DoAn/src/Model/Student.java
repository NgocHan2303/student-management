/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.util.Date;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author MinhDuong
 */
public class Student {

    private String maHV;
    private String ho;
    private String ten;
    private Date ngSinh;
    private String gioiTinh;
    private String noiSinh;
    private String maLop;
    private ArrayList<Double> diemList;
    private double averageDiem;

    public void setMaHV(String maHV) {
        this.maHV = maHV;
    }
    
    public String getMaHV() {
        return maHV;
    }

    public void setHo(String ho) {
        this.ho = ho;
    }
    
    public String getHo() {
        return ho;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }
    
    public String getTen() {
        return ten;
    }

    public void setNgSinh(Date ngSinh) {
        this.ngSinh = ngSinh;
    }

    public void setGioiTinh(String gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public void setNoiSinh(String noiSinh) {
        this.noiSinh = noiSinh;
    }

    public void setMaLop(String maLop) {
        this.maLop = maLop;
    }

    public void fetchGradesFromDatabase(MSSQLDatabaseConnection sqlConnection) {
        ArrayList<Double> grades = new ArrayList<>();

        String sql = "SELECT DIEM FROM KETQUATHI WHERE MAHV = ?";

        try {
            ResultSet rs = sqlConnection.executeQuery(sql);
            while (rs.next()) {
                double diem = rs.getDouble("DIEM");
                grades.add(diem);
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        diemList = grades;
    }

    public void calAverageDiem() {
        if (diemList.isEmpty()) {
            averageDiem = 0.0;
        } else {
            double totalDiem = 0.0;
            for (Double diem : diemList) {
                totalDiem += diem;
            }
            averageDiem = totalDiem / diemList.size();
        }
    }

    public void setAverageDiem(double averageDiem) {
        this.averageDiem = averageDiem;
    }

    public double getAverageDiem() {
        return averageDiem;
    }
}
