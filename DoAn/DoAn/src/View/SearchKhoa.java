/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package View;

import Model.MSSQLDatabaseConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.sql.ResultSetMetaData;

/**
 *
 * @author MinhDuong
 */
public class SearchKhoa extends javax.swing.JFrame {

    private MSSQLDatabaseConnection sqlConnection;

    /**
     * Creates new form SearchGV
     */
    public SearchKhoa(MSSQLDatabaseConnection sqlConnection) {
        show();
        initComponents();
        this.sqlConnection = sqlConnection;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtInputKhoa = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        btnSearchMaKhoa = new javax.swing.JButton();
        btnSearchTenKhoa = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(253, 240, 213));

        jPanel1.setBackground(new java.awt.Color(253, 240, 213));

        txtInputKhoa.setText("Nhập mã khoa/Tên khoa");

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel1.setText("TÌM KIẾM KHOA");

        btnSearchMaKhoa.setBackground(new java.awt.Color(224, 159, 62));
        btnSearchMaKhoa.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnSearchMaKhoa.setForeground(new java.awt.Color(253, 240, 213));
        btnSearchMaKhoa.setText("Tìm theo mã khoa");
        btnSearchMaKhoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchMaKhoaActionPerformed(evt);
            }
        });

        btnSearchTenKhoa.setBackground(new java.awt.Color(56, 102, 65));
        btnSearchTenKhoa.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        btnSearchTenKhoa.setForeground(new java.awt.Color(253, 240, 213));
        btnSearchTenKhoa.setText("Tìm theo tên khoa");
        btnSearchTenKhoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchTenKhoaActionPerformed(evt);
            }
        });

        tbl1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tbl1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(75, 75, 75)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(btnSearchMaKhoa)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnSearchTenKhoa))
                                    .addComponent(txtInputKhoa, javax.swing.GroupLayout.PREFERRED_SIZE, 378, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(207, 207, 207)
                                .addComponent(jLabel1)))
                        .addGap(0, 69, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(txtInputKhoa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSearchMaKhoa)
                    .addComponent(btnSearchTenKhoa))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSearchMaKhoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchMaKhoaActionPerformed
        String input = txtInputKhoa.getText();
        String sql = "SELECT * FROM KHOA WHERE MAKHOA = '" + input + "'";

        try {
            ResultSet rs = sqlConnection.executeQuery(sql);

            if (rs.next()) {
                DefaultTableModel tblModel = (DefaultTableModel) tbl1.getModel();
                tblModel.setRowCount(0);

                ResultSetMetaData rsmd = rs.getMetaData();
                int numColumns = rsmd.getColumnCount();

                for (int i = 1; i <= numColumns; i++) {
                    tblModel.addColumn(rsmd.getColumnName(i));
                }

                Object[] rowData = new Object[numColumns];
                do {
                    for (int i = 1; i <= numColumns; i++) {
                        rowData[i - 1] = rs.getString(i);
                    }
                    tblModel.addRow(rowData);
                } while (rs.next());
            } else {
                JOptionPane.showMessageDialog(null, "Không tìm thấy khoa với mã " + input);
            }
            this.setDefaultCloseOperation(this.HIDE_ON_CLOSE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnSearchMaKhoaActionPerformed

    private void btnSearchTenKhoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchTenKhoaActionPerformed
        String input = txtInputKhoa.getText();
        String sql = "SELECT * FROM KHOA WHERE TENKHOA LIKE '%" + input + "%'";

        try {
            ResultSet rs = sqlConnection.executeQuery(sql);

            if (rs.next()) {
                DefaultTableModel tblModel = (DefaultTableModel) tbl1.getModel();
                tblModel.setRowCount(0);

                ResultSetMetaData rsmd = rs.getMetaData();
                int numColumns = rsmd.getColumnCount();

                for (int i = 1; i <= numColumns; i++) {
                    tblModel.addColumn(rsmd.getColumnName(i));
                }

                Object[] rowData = new Object[numColumns];
                do {
                    for (int i = 1; i <= numColumns; i++) {
                        rowData[i - 1] = rs.getString(i);
                    }
                    tblModel.addRow(rowData);
                } while (rs.next());
                this.setDefaultCloseOperation(this.HIDE_ON_CLOSE);  
            } else {
                JOptionPane.showMessageDialog(null, "Không tìm thấy khoa với tên " + input);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_btnSearchTenKhoaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSearchMaKhoa;
    private javax.swing.JButton btnSearchTenKhoa;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbl1;
    private javax.swing.JTextField txtInputKhoa;
    // End of variables declaration//GEN-END:variables
}
